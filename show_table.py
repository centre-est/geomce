# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GeoMCE version 2.0
                                 A QGIS plugin
 GeoMCE permet de mettre en forme des couches shape en vue des les impoprter
 dans la plateforme nationale de géolocalisation des mesures compensatoires
 environnementales prévue par la loi sur la reconquète de la biodiveristé et
 des paysages du 16 aout 2016
                              -------------------
        begin                : 2020-03-26
        git sha              : $Format:%H$
        copyright            : (C) 2020 by DREAL Auvergne-Rhône-Alpes - 2024 by Cerema Centre-Est
        email                : antoine.lemot@cerema.fr
 ***************************************************************************/
"""

from PyQt5.QtWidgets import QFileDialog, QMessageBox
from qgis.core import *
from qgis.gui import *
from qgis.utils import *
from osgeo import ogr, osr
import sys, os, os.path, sqlite3, shutil, csv, subprocess, unicodedata
import processing
from processing.tools import dataobjects

os.putenv("SPATIALITE_SECURITY", "relaxed")

class Montrer_Table():

    def show_table(self):
        """Ouverture de la table attributaire de la couche qui a ete traiter

        Cette fonction permet permet d afficher la table attributaire de la couche qui a ete traiter
        (fonction déclenchée par bouton 'Afficher la nouvelle table attributaire' de l'onglet 'Mise en forme de vecteurs existants')
        """

        table_layer = self.mMapLayerComboBox_3.currentText()
        for name, selectlayer in QgsProject.instance().mapLayers().items():
            if selectlayer.name() == table_layer:
                show_layer_t = selectlayer
                self.iface.showAttributeTable(show_layer_t)
