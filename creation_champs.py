# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GeoMCE version 2.0
                                 A QGIS plugin
 GeoMCE permet de mettre en forme des couches shape en vue des les impoprter
 dans la plateforme nationale de géolocalisation des mesures compensatoires
 environnementales prévue par la loi sur la reconquète de la biodiveristé et
 des paysages du 16 aout 2016
                              -------------------
        begin                : 2020-03-26
        git sha              : $Format:%H$
        copyright            : (C) 2020 by DREAL Auvergne-Rhône-Alpes - 2024 by Cerema Centre-Est
        email                : antoine.lemot@cerema.fr
 ***************************************************************************/
"""

from PyQt5.QtWidgets import QFileDialog, QMessageBox
from PyQt5.QtCore import *
from qgis.core import *
from osgeo import ogr, osr
import sys, os, os.path, sqlite3, shutil, csv, subprocess, unicodedata
import processing
from processing.tools import dataobjects
from .boite import Boite

os.putenv("SPATIALITE_SECURITY", "relaxed")


class Creer_Champs():

    def create_new_field_2_f(self):
        """Création des champs compatibles avec GeomCE

        Cette fonction sert à créer les champs compatibles avec GeomCE dans la couche choisie
        (fonction déclenchée par bouton 'Création de champs compatibles avec GeoMCE')
        """

        layer = self.mMapLayerComboBox_3.currentLayer()
        source_chemin_couche = layer.dataProvider().dataSourceUri()
        layer.startEditing()
        #on efface tout les champs existants...
        prov = layer.dataProvider()
        field_names = [field.name() for field in prov.fields()]
        for count, f in enumerate(field_names):
            jean = range(count, count +1 )
            paul = range(count)
            jeanpaul = list(range(count, count +1 )) + list (range(count))
        layer.dataProvider().deleteAttributes(jeanpaul)

        #... et creation des nouveaux champs compatibles avec un import dans GeoMCE

        layer.dataProvider().addAttributes(
            [QgsField("id", QVariant.Int,'Integer64',10,0),
             QgsField("nom", QVariant.String,'String',100,0),
             QgsField("categorie", QVariant.String,'String',7,0),
             QgsField("cible", QVariant.String,'String',254,0),
             QgsField("descriptio", QVariant.String,'String',254,0),
             QgsField("decision", QVariant.String,'String',254,0),
             QgsField("refei", QVariant.String,'String',254,0),
             QgsField("duree", QVariant.String,'String',25,0),
             QgsField("unite", QVariant.String,'String',25,0),
             QgsField("modalite", QVariant.String,'String',50,0),
             QgsField("cout",QVariant.Int,'Integer64', 10,0),
             QgsField("montant_pr",QVariant.Int,'Integer64', 10,0),
             QgsField("faunes",QVariant.String, 'String', 254,0),
             QgsField("flores",QVariant.String, 'String', 254,0),
             QgsField("echeances",QVariant.String, 'String', 254,0),
             QgsField("communes", QVariant.String,'String',220,0)])

        layer.updateFields()
        layer.selectAll()
        Boite.message(self, u"Tout les champs ont été effacés ",
                      u" -> j\'espère que tu ne le regretteras pas!").exec_()
        self.compatibilite()
        return