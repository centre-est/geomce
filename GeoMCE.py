# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GeoMCE version 2.0
                                 A QGIS plugin
 GeoMCE permet de mettre en forme des couches shape en vue des les impoprter
 dans la plateforme nationale de géolocalisation des mesures compensatoires
 environnementales prévue par la loi sur la reconquète de la biodiveristé et
 des paysages du 16 aout 2016
                              -------------------
        begin                : 2020-03-26
        git sha              : $Format:%H$
        copyright            : (C) 2020 by DREAL Auvergne-Rhône-Alpes - 2024 by Cerema Centre-Est
        email                : antoine.lemot@cerema.fr
 ***************************************************************************/
"""
from __future__ import absolute_import
from builtins import str
from builtins import range
from builtins import object
from PyQt5.QtCore import *
from PyQt5 import QtWidgets
from PyQt5.QtGui import *
from qgis.core import *
from qgis.gui import *
from qgis.PyQt.QtGui import *
from qgis.utils import *
from PyQt5.QtWidgets import QAction, QDialog, QFormLayout, QFileDialog, QProgressBar, QListWidgetItem, QGridLayout
import  os, processing, fnmatch, sys, glob, datetime, unicodedata, zipfile, os.path, urllib
# Import the code for the dialog
from .GeoMCE_dialog import GeoMCEDialog
from .aboutdialog import AboutDialog
from .resources import *
from os.path import basename
from datetime import date
from pathlib import Path


### Import des modules complementaires
from .interface import *

class GeoMCE: #(object):
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgisInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface

        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'GeoMCE_v2{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = AppelFonction(iface)

        # self.dlg = GeoMCEDialog()
        # self.dlga = AboutDialog()
        # #initialization for select features
        # self.turnedoffLayers = []
        # self.selectList = []
        # self.cLayer = None
        # self.provider = None
        # self.saved = False
        # self.countchange = 0
        # selectall = 0

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&GeoMCE_v2')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'GeoMCE_v2')
        self.toolbar.setObjectName(u'GeoMCE_v2')


    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('GeoMCE_v2', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        # Create the dialog (after translation) and keep reference
        #self.dlg = GeoMCEDialog()
        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToVectorMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS G"""


        icon_path = ':/plugins/GeoMCE_v2/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'GeoMCE_v2'),
            callback=self.run,
            parent=self.iface.mainWindow())

        self.pluginIsActive = None

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(self.tr(u'&GeoMCE_v2'),action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar

    def run(self):
        """Run method that performs all the real work"""
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # self.dlg.mMapLayerComboBox_4.setCurrentIndex(0)
        # See if OK was pressed

        if result == 0 :
            self.dlg.nom.clear()
            self.dlg.reject()
            self.dlg.description.clear()
            self.dlg.decision.clear()
            self.dlg.refei.clear()
            self.dlg.duree.clear()
            self.dlg.comboBox.clear()
            self.dlg.comboBox_2.clear()
            self.dlg.comboBox_3.clear()
            self.dlg.comboBox_4.clear()
            self.dlg.cout.clear()
            self.dlg.montant.clear()
            self.dlg.faunes.clear()
            self.dlg.flores.clear()
            self.dlg.echeances.clear()
            self.dlg.mMapLayerComboBox_4.setCurrentIndex(0)
            self.dlg.mMapLayerComboBox_3.setCurrentIndex(0)
            self.dlg.mMapLayerComboBox_3.setStyleSheet('QgsMapLayerComboBox {border: 1px solid ;border - radius: 3px; '
                                                   'background: white;padding: 1px 23px 1px 3px; min - width: 6em;'
                                                   'background - color: rgb(255, 255, 255);'
                                                   'selection - background - color: rgb(0, 170, 255)}')
            self.dlg.label_9.setStyleSheet('QLabel {color: black')

            self.dlg.mMapLayerComboBox_4.setCurrentIndex(0)
            self.dlg.mFieldComboBox_2.setLayer(self.dlg.mMapLayerComboBox_4.setCurrentIndex(0))
            self.dlg.mMapLayerComboBox_3.setCurrentIndex(0)
            self.dlg.mComboBox.clear()
            layers = [layer for layer in QgsProject.instance().mapLayers().values()]
            for layer in layers:
                layerType = layer.type()
                if layerType == QgsMapLayer.VectorLayer:
                    self.dlg.mComboBox.addItem(layer.name())

            self.dlg.enregistrement.clear()
            self.dlg.enregistrement_fusion.clear()
            self.dlg.g_l.setChecked(False)
            self.dlg.g_pt.setChecked(False)
            self.dlg.g_poly.setChecked(False)
            self.dlg.g_c.setChecked(False)
            self.dlg.tabWidget.setCurrentWidget(self.dlg.tab_2)

            self.dlg.enregistrement_fusion.setEnabled(False)
            self.dlg.pushButton_4.setEnabled(False)
            self.dlg.fusion.setEnabled(False)
            self.dlg.mFieldComboBox_2.setEnabled(False)
            self.dlg.enregistrement.setEnabled(False)
            self.dlg.create_new_field.setEnabled(False)
            self.dlg.pushButton_3.setEnabled(False)

            self.dlg.label_27.hide()
            self.dlg.label_28.show()
            self.dlg.nom.setEnabled(False)
            self.dlg.comboBox.setEnabled(False)
            self.dlg.comboBox_2.setEnabled(False)
            self.dlg.decision.setEnabled(False)
            self.dlg.refei.setEnabled(False)
            self.dlg.description.setEnabled(False)
            self.dlg.echeances.setEnabled(False)
            self.dlg.montant.setEnabled(False)
            self.dlg.cout.setEnabled(False)
            self.dlg.duree.setEnabled(False)
            self.dlg.comboBox_3.setEnabled(False)
            self.dlg.comboBox_4.setEnabled(False)
            self.dlg.faunes.setEnabled(False)
            self.dlg.flores.setEnabled(False)
            self.dlg.change_another.setEnabled(False)
            self.dlg.save.setEnabled(False)
            self.dlg.zipbutton.setEnabled(False)
            self.dlg.eff_form.setEnabled(False)
            self.dlg.affiche_form.setEnabled(False)

            # Do something useful here - delete the line containing pass and
            # substitute with your code.
            pass

    def unload(self):
        """Removes the plugin menu item and icon from QGIS G"""
        for action in self.actions:
            self.iface.removePluginVectorMenu(
                self.tr(u'&GeoMCE'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar


