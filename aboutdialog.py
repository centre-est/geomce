"""
/***************************************************************************
 GeoMCE version 2.0
                                 A QGIS plugin
 GeoMCE permet de mettre en forme des couches shape en vue des les impoprter
 dans la plateforme nationale de géolocalisation des mesures compensatoires
 environnementales prévue par la loi sur la reconquète de la biodiveristé et
 des paysages du 16 aout 2016
                              -------------------
        begin                : 2020-03-26
        git sha              : $Format:%H$
        copyright            : (C) 2020 by DREAL Auvergne-Rhône-Alpes - 2024 by Cerema Centre-Est
        email                : antoine.lemot@cerema.fr
 ***************************************************************************/
"""

from PyQt5 import QtCore, QtGui, QtGui, uic, QtWidgets
from .about import Ui_About
import sys
import webbrowser
import os.path

# create the dialog
class AboutDialog(QtWidgets.QDialog, Ui_About):

    def __init__(self,parent = None) :
        super(AboutDialog, self).__init__(parent)
        self.setupUi(self)
        self.parent = parent
        self.label_3 = self.label_3
        self.label_3.clicked.connect(self.initialize)
        self.label_4 = self.label_4
        self.label_4.clicked.connect(self.load_guide)


    def initialize(self) :
        self.close()
        self.dlga0 = AboutDialog()
        self.dlga0.show()

    def load_guide(self):
        """Chargement du PDF dans l'extension

        Cette fonction permet de charger et de visualiser le pdf dans About
        """
        path_manuel = os.path.join(os.path.dirname(__file__), 'img', '20240701_Notice_plug_in.pdf')
        path_manuel = path_manuel.replace('\\', '/')
        webbrowser.open(path_manuel)



        