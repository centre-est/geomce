# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GeoMCE version 2.0
                                 A QGIS plugin
 GeoMCE permet de mettre en forme des couches shape en vue des les impoprter
 dans la plateforme nationale de géolocalisation des mesures compensatoires
 environnementales prévue par la loi sur la reconquète de la biodiveristé et
 des paysages du 16 aout 2016
                              -------------------
        begin                : 2020-03-26
        git sha              : $Format:%H$
        copyright            : (C) 2020 by DREAL Auvergne-Rhône-Alpes - 2024 by Cerema Centre-Est
        email                : antoine.lemot@cerema.fr
 ***************************************************************************/
"""

from PyQt5.QtWidgets import QFileDialog, QMessageBox
from qgis.core import *
from osgeo import ogr, osr
import sys, os, os.path, sqlite3, shutil, csv, subprocess, unicodedata, zipfile
import processing
from os.path import basename
from datetime import date
from pathlib import Path

from processing.tools import dataobjects
from .boite import Boite

os.putenv("SPATIALITE_SECURITY", "relaxed")


class Zipper():

    def archive(self, path):
        """Zippage de la table traitée

        Cette fonction permet de zipper la table traitée afin de l'exporter dans l'outil GeoMCE
        (fonction déclenchée par bouton 'Créer une archive .zip' de l'onglet 'Mise en forme de vecteurs existants')
        """

        chemin_couche = self.mMapLayerComboBox_3.currentLayer()
        source_chemin_couche = chemin_couche.dataProvider().dataSourceUri()
        chemin_zip = os.path.dirname(source_chemin_couche)
        nom_couche = self.mMapLayerComboBox_3.currentText()

        nom_zip = self.get_new_nom().replace(" ","_")
        if len(nom_zip) > 10 :
            Boite.message(self,
                          u'le nom du fichier zip fait plus de 10 caractères (' + str(len(nom_zip)) + u')',
                          u" ->le nom a été tronqué pour créer une archive importable dans GéoMCE").exec_()
            nom_zip = nom_zip[:10]
            pass

        else :
            nom_zip = self.get_new_nom().replace(" ","_")
            pass

        nom_zip_complet = chemin_zip + "/" + nom_zip + '.zip'
        nom_ok = nom_zip_complet
        f_shp = chemin_zip + "/" + nom_couche + '.shp'
        f_shpPath = Path(f_shp)
        archive = zipfile.ZipFile(nom_ok, mode='w')
        f_shp = chemin_zip + "/" + nom_couche + '.shp'
        f_shpPath = Path(f_shp)
        f_shx = chemin_zip + "/" + nom_couche + '.shx'
        f_shxPath = Path(f_shx)
        f_cpg = chemin_zip + "/" +  nom_couche + '.cpg'
        f_cpgPath= Path(f_cpg)
        f_dbf = chemin_zip + "/" +  nom_couche + '.dbf'
        f_dbfPath = Path(f_dbf)
        f_prj = chemin_zip + "/" +  nom_couche + '.prj'
        f_prjPath = Path(f_prj)

        if f_shpPath.exists() and f_shxPath.exists() and f_dbfPath.exists():
            archive.write(f_shp,basename(f_shp))
            archive.write(f_shx,basename(f_shx))
            archive.write(f_dbf,basename(f_dbf))
        else:
            Boite.message(self, u'Il manque un fichier' + f_shp + ' ou  '+f_dbf+ ' ou '+ f_shx +u' pour créer une archive importable dans GéoMCE',
                          u" -> modifiez la couche").exec_()
            return

        if f_cpgPath.exists() :
            archive.write(f_cpg,basename(f_cpg))
        else:
            pass
        if f_prjPath.exists() :
            archive.write(f_prj,basename(f_prj))
        else:
            pass
        archive.close()

        Boite.message(self,
                  u'Les fichiers shp + cpg + dbf + prj + shx ont été ajoutés dans l\'archive ' + nom_ok,
                  u" -> Ils sont dans le dossier : " + chemin_zip ).exec_()
