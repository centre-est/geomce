# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GeoMCE version 2.0
                                 A QGIS plugin
 GeoMCE permet de mettre en forme des couches shape en vue des les impoprter
 dans la plateforme nationale de géolocalisation des mesures compensatoires
 environnementales prévue par la loi sur la reconquète de la biodiveristé et
 des paysages du 16 aout 2016
                              -------------------
        begin                : 2020-03-26
        git sha              : $Format:%H$
        copyright            : (C) 2020 by DREAL Auvergne-Rhône-Alpes - 2024 by Cerema Centre-Est
        email                : antoine.lemot@cerema.fr
 ***************************************************************************/
"""

from PyQt5.QtWidgets import QFileDialog, QMessageBox
from qgis.core import *
import os.path, unicodedata

class Boite :

   def message(self,texte,info):
      """Création d'une boite de dialogue

      Cette fonction permet de créer une boite de dialogue qui sera affichée lorsqu'une erreur dans l'utilisation
      de l'extension sera relevée
      """

      msg1 = QMessageBox()
      msg1.setIcon(QMessageBox.Warning)
      msg1.setText(texte)
      msg1.setInformativeText(info)
      msg1.setWindowTitle(u"Avertissement")
      return msg1

