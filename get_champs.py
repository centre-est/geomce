# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GeoMCE version 2.0
                                 A QGIS plugin
 GeoMCE permet de mettre en forme des couches shape en vue des les impoprter
 dans la plateforme nationale de géolocalisation des mesures compensatoires
 environnementales prévue par la loi sur la reconquète de la biodiveristé et
 des paysages du 16 aout 2016
                              -------------------
        begin                : 2020-03-26
        git sha              : $Format:%H$
        copyright            : (C) 2020 by DREAL Auvergne-Rhône-Alpes - 2024 by Cerema Centre-Est
        email                : antoine.lemot@cerema.fr
 ***************************************************************************/
"""

from PyQt5.QtWidgets import QAction, QDialog, QFormLayout, QFileDialog, QProgressBar, QListWidgetItem, QGridLayout
from PyQt5.QtCore import QSettings
from qgis.core import *
from osgeo import ogr, osr
import sys, os, os.path, sqlite3, shutil, csv, subprocess, unicodedata
import processing
from processing.tools import dataobjects

from .liste_faune import liste_faune,liste_flore,liste_categorie_full,liste_categorie

os.putenv("SPATIALITE_SECURITY", "relaxed")

class Champs():

    def get_new_nom(self):
        """Récupération du nom
        """
        new_nom = self.nom.text()
        return new_nom


    def get_description(self):
        """Récupération de la description
        """
        new_val_description = self.description.toPlainText()
        return new_val_description


    def get_decision(self):
        """Récupération de la décision
        """
        new_val_decicision = self.decision.text()
        return new_val_decicision


    def get_refei(self):
        new_val_refei = self.refei.text()
        return new_val_refei

    def get_duree(self):
        """Récupération de la durée
        """
        new_val_duree = self.duree.text()
        return new_val_duree


    def get_categorie(self):
        """Récupération de la catégorie
        """
        numero_categorie = liste_categorie
        new_val_categorie = self.comboBox.currentIndex()
        index = numero_categorie[new_val_categorie]
        return index


    def get_cible(self):
        """Récupération de la/les cible/s
        """
        cible = [u'Continuités écologiques', u'Bruit', u'Eau', u'Biens matériels', u'Equilibres biologiques',
                 u'Espaces naturels, agricoles, forestiers, maritimes ou de loisirs', u'Facteurs climatiques',
                 u'Faune et flore',
                 u'Habitats naturels', u'Patrimoine culturel et archéologique', u'Population', u'Sites et paysages',
                 u'Sol']
        liste_state = []
        for i in range(0, 13):
            state = self.comboBox_2.itemCheckState(i)
            liste_state.append(state)

        res = {cible[i]: liste_state[i] for i in range(len(cible))}

        list_text = []
        for (key, value) in res.items():
            if value == 2:
                list_text.append(key)
        new_val_cible = "|".join(list_text)
        return new_val_cible


    def get_unite(self):
        """Récupération de l'unité de la durée
        """
        numero_unite = [u'Année', u'Mois', u'Jour']
        new_val_unite = self.comboBox_3.currentIndex()
        index = numero_unite[new_val_unite]
        return index

    def get_modalite(self):
        """Récupération de la modalité
        """
        new_val_modalite = self.comboBox_4.currentText()
        return new_val_modalite

    def get_communes(self):
        """Récupération de la/les commune/s
        """
        s = QSettings()
        new_val_communes = s.value('/GeoMCE-chemin_communes.shp')
        return new_val_communes

    def get_cout(self):
        """Récupération du cout
        """
        new_val_cout = self.cout.text()
        return new_val_cout

    def get_montant(self):
        """Récupération du montant
        """
        new_val_montant = self.montant.text()
        return new_val_montant

    def get_faunes(self):
        """Récupération de la faune
        """
        faunes = liste_faune
        liste_state_faunes = []
        for i in range(0, 1613):
            state = self.faunes.itemCheckState(i)
            liste_state_faunes.append(state)

        res = {faunes[i]: liste_state_faunes[i] for i in range(len(faunes))}

        list_text_faunes = []
        for (key, value) in res.items():
            if value == 2:
                list_text_faunes.append(key)
        new_val_faunes = "|".join(list_text_faunes)
        return new_val_faunes

    def get_flores(self):
        """Récupération de la flore
        """
        flores = liste_flore
        liste_state_flores = []
        for i in range(0, 3332):
            state = self.flores.itemCheckState(i)
            liste_state_flores.append(state)

        res = {flores[i]: liste_state_flores[i] for i in range(len(flores))}

        list_text_flores = []
        for (key, value) in res.items():
            if value == 2:
                list_text_flores.append(key)
        new_val_flores = "|".join(list_text_flores)
        return new_val_flores

    def get_echeances(self):
        """Récupération de l'échéance'
        """
        new_val_echeances = self.echeances.text()
        return new_val_echeances

    def date(self):
        """Récupération de la date
        """

        delivrance = self.delivrance.date()
        date = ""
        for i in new_date_ech:
            date += delivrance.addYears(int(i)).toString(Qt.LocaleDate) + "|"
        return date