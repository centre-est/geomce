# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GeoMCE version 2.0
                                 A QGIS plugin
 GeoMCE permet de mettre en forme des couches shape en vue des les impoprter
 dans la plateforme nationale de géolocalisation des mesures compensatoires
 environnementales prévue par la loi sur la reconquète de la biodiveristé et
 des paysages du 16 aout 2016
                              -------------------
        begin                : 2020-03-26
        git sha              : $Format:%H$
        copyright            : (C) 2020 by DREAL Auvergne-Rhône-Alpes - 2024 by Cerema Centre-Est
        email                : antoine.lemot@cerema.fr
 ***************************************************************************/
"""


from PyQt5.QtWidgets import QFileDialog, QMessageBox
from PyQt5.QtCore import QSettings
from qgis.core import *
from qgis.gui import *
from qgis.PyQt.QtGui import *
from qgis.utils import *
from osgeo import ogr, osr
import  os, processing, fnmatch, sys, glob, datetime, unicodedata, zipfile, os.path, urllib
import processing
from pathlib import Path
from processing.tools import dataobjects
from .boite import Boite

os.putenv("SPATIALITE_SECURITY", "relaxed")

class Ouvrir_Wfs():

    def wfs(self):
        """Chargement des flux WFS des mesures compensatoires

        Cette fonction permet de charger les couches WFS des mesures compensatoires existantes dans Cerema Data
        """

        root = QgsProject.instance().layerTreeRoot()
        groupe = root.addGroup("MC existantes dans la base de donnée nationale")
        if self.g_l.isChecked():
            uri= "https://datacarto.cdata.cerema.fr/wfs?request=GetCapabilities&TYPENAME=ms:geomce_3857_l_mesure_compensatoire_l_000"
            vlayer = QgsVectorLayer(uri, "MC existantes - linéaires", "WFS")
            QgsProject.instance().addMapLayer(vlayer, False)
            groupe.addLayer(vlayer)

        if self.g_pt.isChecked():
            uri2= "https://datacarto.cdata.cerema.fr/wfs?request=GetCapabilities&TYPENAME=ms:geomce_3857_l_mesure_compensatoire_p_000"
            vlayer2 = QgsVectorLayer(uri2, "MC existantes - points", "WFS")
            QgsProject.instance().addMapLayer(vlayer2, False)
            groupe.addLayer(vlayer2)

        if self.g_poly.isChecked():
            uri3= "https://datacarto.cdata.cerema.fr/wfs?request=GetCapabilities&TYPENAME=ms:geomce_3857_l_mesure_compensatoire_s_000"
            vlayer3 = QgsVectorLayer(uri3, "MC existantes - polygones", "WFS")
            QgsProject.instance().addMapLayer(vlayer3, False)
            groupe.addLayer(vlayer3)

        if self.g_c.isChecked():
            uri4= "https://datacarto.cdata.cerema.fr/wfs?request=GetCapabilities&TYPENAME=ms:geomce_3857_l_mesure_compensatoire_commune_000"
            vlayer4 = QgsVectorLayer(uri4, "MC existantes - communes", "WFS")
            QgsProject.instance().addMapLayer(vlayer4, False)
            groupe.addLayer(vlayer4)


class Separer_Couches():

    def enabled_separer(self):
        if self.mMapLayerComboBox_4.currentIndex() != 0 :
            self.mFieldComboBox_2.setEnabled(True)
            self.enregistrement.setEnabled(True)
            self.create_new_field.setEnabled(True)
            self.pushButton_3.setEnabled(True)
        else :
            self.mFieldComboBox_2.setEnabled(False)
            self.enregistrement.setEnabled(False)
            self.create_new_field.setEnabled(False)
            self.pushButton_3.setEnabled(False)

    def listecouches_separer(self):
        """Actualsation de la liste des couches pouvant etre fusionnes

        Cette fonction permet d'actualiser la liste des couches pouvant etre fusionnes
        """

        map_layers = QgsProject.instance().mapLayers().values()
        allow_list = [
            lyr.id() for lyr in map_layers if lyr.type() == QgsMapLayer.VectorLayer and lyr.featureCount() > 1]
        except_list = [l for l in map_layers if l.id() not in allow_list]
        self.mMapLayerComboBox_4.setExceptedLayerList(except_list)


    def champ_sep(self):
        """Remplissage du menu déroulant des champs

        Cette fonction permet de remplir le menu déroulant pour choisir le champ séparateur
        """

        vlayer = self.mMapLayerComboBox_4.currentLayer()
        self.mFieldComboBox_2.setLayer(vlayer)

    def select_save_folder(self):
        """Choix du chemin des couches séparées

        Cette fonction permet de choisir le chemin dans lequel les couches séparées seront créées
        """

        qfd_1 = QFileDialog()
        title_1 = u'T\'enregistres ça où?'
        path_1 = ""
        filename = QFileDialog.getExistingDirectory(qfd_1, title_1, path_1)
        self.enregistrement.setText(filename)

    def get_enregistrement(self):
        """Enregistrement du chemin des couches séparées

        Cette fonction permet d'enregistrer le chemin dans lequel les couches séparées seront créées.
        Si le champs n'est pas rempli, le dossier par défaut (contenant la couche à éclatée) est choisi
        """

        chemin_couche = self.mMapLayerComboBox_4.currentLayer()
        source_chemin_couche = chemin_couche.dataProvider().dataSourceUri()
        chemin_couche_separer = os.path.dirname(source_chemin_couche)
        if self.enregistrement.text() == "":
            new_enregistrement = chemin_couche_separer
        else:
            new_enregistrement = self.enregistrement.text()
        return new_enregistrement


    def create_new_field_f(self):
        """Séparation d'une couche vecteur

        Cette fonction permet de séparer (d'éclater) une couche vecteur en autant de géométries qu'elle contient
        Elle utilise l'algorythme qgis:splitvectorlayer
        """

        # creation d un dossier qui va porter le nom de la couche + le nom du champ separateur qui va etre traiter
        couche = self.mMapLayerComboBox_4.currentText()
        champ = self.mFieldComboBox_2.currentText()
        path = self.get_enregistrement()
        p = path + "/" + datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + "_" + couche + "_" + champ
        os.makedirs(p)


        try:
            split = processing.run("qgis:splitvectorlayer", {'INPUT':couche,
                                                         'FIELD': champ, 'FILE_TYPE': 1,
                                                         'OUTPUT': p})
        except QgsProcessingException as error:
            Boite.message(self, str(error).split('!')[0],
                         u"\n -> Modifiez avant de relancer").exec_()
            return

        # permet de scanner le dossier de get_enregistrement
        def find_files(directory, pattern, only_root_directory):
            for root, dirs, files in os.walk(directory):
                for basename in files:
                    if fnmatch.fnmatch(basename.lower(), pattern):
                        filename = os.path.join(root, basename)
                        yield filename
                if (only_root_directory):
                    break

        # puis on réenregistre en UTF-8 et on affiche les shp créés
        liste_nvelles_couches = []
        for src_file in find_files(p, '*.shp', True):

            (head, tail) = os.path.split(src_file)
            (name, ext) = os.path.splitext(tail)

            src_file_utf8 = head + "/" + name + "_utf8.shp"
            src_file_layer = QgsVectorLayer(src_file, name, "ogr")

            QgsVectorFileWriter.writeAsVectorFormat(
                src_file_layer, src_file_utf8, 'UTF-8', src_file_layer.crs(), 'ESRI Shapefile')

            (head2, tail2) = os.path.split(src_file_utf8)
            (name2, ext2) = os.path.splitext(tail2)

            vlayer = iface.addVectorLayer(src_file_utf8, name2 , "ogr")

            liste_nvelles_couches.append(name + "_utf8.shp")

        self.mFieldComboBox_2.clear()

        Boite.message(self, str(len(liste_nvelles_couches)) + u' nouvelles couches ' + str(liste_nvelles_couches) + ' ont été créées dans le dossier '
                                            + p,
                           u" -> Elles sont maintenant chargées dans l'interface").exec_()

class Fusionner_Couches():

    def enabled_fusioner(self):

        if len(self.mComboBox.checkedItems()) > 1 :
            self.enregistrement_fusion.setEnabled(True)
            self.pushButton_4.setEnabled(True)
            self.fusion.setEnabled(True)
        else :
            self.enregistrement_fusion.setEnabled(False)
            self.pushButton_4.setEnabled(False)
            self.fusion.setEnabled(False)

    def listecouches_fusion(self):
        """Actualsation de la liste des couches pouvant etre fusionnes

        Cette fonction permet d'actualiser la liste des couches pouvant etre fusionnes
        """

        self.mComboBox.clear()

        layers = [layer for layer in QgsProject.instance().mapLayers().values()]
        for layer in layers:
            layerType = layer.type()
            if layerType == QgsMapLayer.VectorLayer:
                self.mComboBox.addItem(layer.name())

    def select_save_folder_2(self):
        """Choix du chemin et du nom de la couche de fusion

        Cette fonction permet de choisir le chemin et du nom de la couche de fusion
        """

        qfd_1 = QFileDialog()
        filename = QFileDialog.getSaveFileName(qfd_1, "Nom des couches fusionnées","", '*.shp')
        self.enregistrement_fusion.setText(filename[0])


    def fusion_f(self):
        """Fusion des couches de mesure

        Cette fonction permet de fusionner 2 couches de mesures ayant les mêmes géométries et étant déjà au format
        GeoMCE.
        Elle vérifie que :
         - les champs des couches sélectionnées soient compatibles avec GeoMCE
         - les couches séelctionnées ont la même géométrie
        Elle utilise l'algorythme gis:mergevectorlayers"
        """


        layers = self.mComboBox.checkedItems()
        path = self.enregistrement_fusion.text()

        list_attendue = ['id', 'nom', 'categorie', 'cible', 'descriptio', 'decision', 'refei', 'duree', 'unite',
                         'modalite', 'cout', 'montant_pr', 'faunes', 'flores', 'echeances', 'communes']

        if not path:
            Boite.message(self, u"Aucun chemin d'enregistrement n'est renseigné",
                          u"Veuillez spécifier un chemin d'enregistrement").exec_()
            return
        else:
            pass

        for layer in layers :
            l = QgsProject.instance().mapLayersByName(layer)[0]
            l_name = l.name()
            field_names = l.fields().names()
            wkbtype = l.wkbType()

            if field_names != list_attendue :
                Boite.message(self, u"La couche " + str(l_name) + u" comporte les champs : " + str(field_names),
                              u"Pour être compatible, elle doit comporter les champs : " + str(list_attendue)
                              + u"\n \n -> Utilisez la fonction 'Création des champs compatibles'").exec_()
                return
            else :
                pass

        try :
            merge = processing.run("qgis:mergevectorlayers",
                                       {'LAYERS':layers,
                                        'CRS':None,
                                        'OUTPUT':path})
        except QgsProcessingException as error:
            Boite.message(self, str(error).split('!')[0],
                         u"\n -> Modifiez les couches et relancez").exec_()
            return


        vlayer = merge['OUTPUT']
        dede = iface.addVectorLayer(path, "", "ogr")
        dede.startEditing()
        dede.dataProvider().deleteAttributes([16,17])
        dede.commitChanges()

        (head, tail) = os.path.split(path)
        (name, ext) = os.path.splitext(tail)

        Boite.message(self, u'La nouvelle couche de fusion ' + name + ' a été créée dans le dossier '
                                            + path,
                           u" -> Elle est maintenant chargée dans l'interface").exec_()


