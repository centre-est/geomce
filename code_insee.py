# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GeoMCE version 2.0
                                 A QGIS plugin
 GeoMCE permet de mettre en forme des couches shape en vue des les impoprter
 dans la plateforme nationale de géolocalisation des mesures compensatoires
 environnementales prévue par la loi sur la reconquète de la biodiveristé et
 des paysages du 16 aout 2016
                              -------------------
        begin                : 2020-03-26
        git sha              : $Format:%H$
        copyright            : (C) 2020 by DREAL Auvergne-Rhône-Alpes - 2024 by Cerema Centre-Est
        email                : antoine.lemot@cerema.fr
 ***************************************************************************/
"""

from PyQt5.QtWidgets import QFileDialog, QMessageBox
from PyQt5.QtCore import QSettings
from qgis.core import *
from osgeo import ogr, osr
import sys, os, os.path, sqlite3, shutil, csv, subprocess, unicodedata
import processing
from processing.tools import dataobjects
from .get_champs import Champs
from .boite import Boite

os.putenv("SPATIALITE_SECURITY", "relaxed")


class Ecrire_Codeinsee():

    def select_output_file_store(self, text):
        """Sélection de la couche des communes

        Cette fonction permet d'aller chercher le fichier commune.shp qui permettra de recuperer le code insee
        et d'enregistrer le chemin du .shp dans le fichier de configuration de QGIS
        (fonction déclenchée par bouton de recherche de dossier dans 'Paramètre de l'extension')
        """

        s = QSettings()
        filename, _filter = QFileDialog.getOpenFileName(self, u'Il est où ton fichier Commune.shp?', "", '*.shp')
        self.communes.setText(filename)
        s.setValue('/GeoMCE-chemin_communes.shp', filename)
        self.iface.messageBar().pushMessage(u'GéoMCE',
                                            u'La couche ' + filename + u' est paramétrée comme couche COMMUNES par défaut',
                                            level=Qgis.Info, duration=3)

class Recuperer_Codeinsee():

    def codeinsee(self):
        """Récupération des codes insee des communes intersectées pr la mesure

        Cette fonction permet de récupérer le code insee des communes qui sont intersectées par la mesure en
        utilisant l'algorythme native:extractbylocation
        """

        layer5 = self.mMapLayerComboBox_3.currentLayer()
        val15 = self.get_communes()
        layer_communes_region = QgsVectorLayer(val15, "communes", "ogr")

        extraction = processing.run('native:extractbylocation',{'INPUT' : layer_communes_region, 'INTERSECT' :layer5, 'PREDICATE' :[0], 'OUTPUT' :'TEMPORARY_OUTPUT'})
        layer_extraction = extraction['OUTPUT']

        type = layer_extraction.dataProvider().fieldNameIndex('TYPE')
        insee =layer_extraction.dataProvider().fieldNameIndex('INSEE_COM')
        dep = layer_extraction.dataProvider().fieldNameIndex('INSEE_DEP')
        compare_field_index = type
        concat_field_index = insee
        new_field_index = dep
        list_com = []
        list_id = []
        feature_dict = {f.id(): f for f in layer_extraction.getFeatures()}

        # Si la couche des communes n'intersectent pas la couche de la mesure : message d'alerte
        if not feature_dict :
            Boite.message(self, u"La couche des communes n'intersecte pas la couche de la mesure ",
                          u" -> Vérifier la couche des communes dans l'onglet 'Paramètres supplémentaires'").exec_()
            return

        # Sinon le numéro de la commune est récupéré
        else :

            for f in feature_dict.values():
                new_field_text = f[concat_field_index]
                list_com.append(new_field_text)

            if len(list_com) > 1 :
                new_field_text = ""
                for field in list_com:
                    new_field_text += "|" + field
                    new_field_text = new_field_text.lstrip('|')
            else :
                new_field_text

            return new_field_text
