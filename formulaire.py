# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GeoMCE version 2.0
                                 A QGIS plugin
 GeoMCE permet de mettre en forme des couches shape en vue des les impoprter
 dans la plateforme nationale de géolocalisation des mesures compensatoires
 environnementales prévue par la loi sur la reconquète de la biodiveristé et
 des paysages du 16 aout 2016
                              -------------------
        begin                : 2020-03-26
        git sha              : $Format:%H$
        copyright            : (C) 2020 by DREAL Auvergne-Rhône-Alpes - 2024 by Cerema Centre-Est
        email                : antoine.lemot@cerema.fr
 ***************************************************************************/
"""

from PyQt5.QtWidgets import QAction, QDialog, QFormLayout, QFileDialog, QProgressBar, QListWidgetItem, QGridLayout
from PyQt5.QtCore import QSettings
from qgis.core import *
from osgeo import ogr, osr
import sys, os, os.path, sqlite3, shutil, csv, subprocess, unicodedata
import processing
from processing.tools import dataobjects

from .code_insee import Recuperer_Codeinsee
from .liste_faune import liste_faune,liste_flore,liste_categorie_full,liste_categorie
from .get_champs import Champs
from .boite import Boite

os.putenv("SPATIALITE_SECURITY", "relaxed")

class Remplir_Formulaire():


    def eff_formulaire(self):
        """Mettre à zéro le formulaire

        Cette fonction permet de mettre à jour le formulaire en effacant l'ensemble des champs si ils sont préremplis
        (fonction déclenchée par bouton 'Effacer le formulaire')
        """

        self.nom.clear()
        self.description.clear()
        self.decision.clear()
        self.refei.clear()
        self.duree.clear()
        self.comboBox.clear()
        self.comboBox_2.clear()
        self.comboBox_3.clear()
        self.comboBox_4.clear()
        self.cout.clear()
        self.montant.clear()
        self.faunes.clear()
        self.flores.clear()
        self.echeances.clear()
        self.champs()

    def change_to_any(self):
        """Application des nouvelles valeurs dans la nouvelle table attributaire

        Cette fonction permet de remplir les champs de la table attributaire comme remplis dans le formulaire
        (fonction déclenchée par bouton 'Ecrire les nouvelles valeurs')
        """

        kiki = self.mMapLayerComboBox_3.currentLayer()
        source_chemin_couche = kiki.dataProvider().dataSourceUri()
        (head, tail) = os.path.split(source_chemin_couche)
        (name, ext) = os.path.splitext(tail)
        format = ext.split('|', 1)

        kiki.startEditing()
        source_chemin_couche = kiki.dataProvider().dataSourceUri()
        chemin_tampon = os.path.dirname(source_chemin_couche)
        nom_couche = self.mMapLayerComboBox_3.currentText()
        layer = self.mMapLayerComboBox_3.currentLayer()

        # on recupere l ensemble des valeurs saisies
        val1 = self.get_new_nom()
        val2 = self.get_description()
        val3 = self.get_decision()
        val4 = self.get_refei()
        val5 = self.get_duree()
        val6 = self.get_categorie()
        val7 = self.get_cible()
        val8 = self.get_unite()
        val9 = self.get_modalite()
        val10 = self.get_cout()
        val11 = self.get_montant()
        val12 = self.get_faunes()
        val13 = self.get_flores()
        val14 = self.get_echeances()
        val15 = self.codeinsee()
        # valdate = self.date()
        # val22 = valdate[:-1]

        for feat in layer.getFeatures():
            layer.changeAttributeValue(feat.id(), 0, feat.id())
            if val1 == "":
                layer.changeAttributeValue(feat.id(), 1, "-")
            else:
                layer.changeAttributeValue(feat.id(), 1, val1)
            if val6 == "":
                layer.changeAttributeValue(feat.id(), 2, "E")
            else:
                layer.changeAttributeValue(feat.id(), 2, val6)
            if val7 == "":
                layer.changeAttributeValue(feat.id(), 3, "Habitats naturels")
            else:
                layer.changeAttributeValue(feat.id(), 3, val7)
            if val2 == "":
                layer.changeAttributeValue(feat.id(), 4, "-")
            else:
                layer.changeAttributeValue(feat.id(), 4, val2)
            if val3 == "":
                layer.changeAttributeValue(feat.id(), 5, "-")
            else:
                layer.changeAttributeValue(feat.id(), 5, val3)
            if val4 == "":
                layer.changeAttributeValue(feat.id(), 6, "-")
            else:
                layer.changeAttributeValue(feat.id(), 6, val4)
            if val5 == "":
                layer.changeAttributeValue(feat.id(), 7, "-")
            else:
                layer.changeAttributeValue(feat.id(), 7, val5)
            layer.changeAttributeValue(feat.id(), 8, val8)
            layer.changeAttributeValue(feat.id(), 9, val9)
            if val10 == "":
                layer.changeAttributeValue(feat.id(), 10, "")
            else:
                layer.changeAttributeValue(feat.id(), 10, val10)
            if val11 == "":
                layer.changeAttributeValue(feat.id(), 11, "")
            else:
                layer.changeAttributeValue(feat.id(), 11, val11)
            if val12 == "":
                layer.changeAttributeValue(feat.id(), 12, "")
            else:
                layer.changeAttributeValue(feat.id(), 12, val12)
            if val13 == "":
                layer.changeAttributeValue(feat.id(), 13, "")
            else:
                layer.changeAttributeValue(feat.id(), 13, val13)
            if val14 == "":
                layer.changeAttributeValue(feat.id(), 14, "")
            else:
                layer.changeAttributeValue(feat.id(), 14, val14)
            if self.get_communes() == "":
                layer.changeAttributeValue(feat.id(), 15, "")
            else:
                layer.changeAttributeValue(feat.id(), 15, val15)


        layername = self.mMapLayerComboBox_3.currentText()
        Boite.message(self, u'Les nouvelles valeurs ont été écrites dans la couche ' + layername,
                      u"-> Vous pouvez le vérifier en affichant la table attributaire puis l'enregistrer").exec_()

    def save_edits(self):
        """Sauvegarde des elements saisis dans la table attributaire

        Cette fonction permet de sauvegarder la nouvelle table attributaire
        (fonction déclenchée par bouton 'Sauvegarder les modifications')
        """

        self.saved = False
        layername = self.mMapLayerComboBox_3.currentText()
        for name, selectlayer in QgsProject.instance().mapLayers().items():
            if selectlayer.name() == layername:
                if (selectlayer.isEditable()):
                    selectlayer.commitChanges()
                    self.saved = True
                    self.countchange = 0

        Boite.message(self, u'Les nouvelles valeurs ont été enregistrées dans la couche ' + layername,
                           u"").exec_()

        return self.saved

