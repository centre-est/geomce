# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GeoMCE version 2.0
                                 A QGIS plugin
 GeoMCE permet de mettre en forme des couches shape en vue des les impoprter
 dans la plateforme nationale de géolocalisation des mesures compensatoires
 environnementales prévue par la loi sur la reconquète de la biodiveristé et
 des paysages du 16 aout 2016
                              -------------------
        begin                : 2020-03-26
        git sha              : $Format:%H$
        copyright            : (C) 2020 by DREAL Auvergne-Rhône-Alpes - 2024 by Cerema Centre-Est
        email                : antoine.lemot@cerema.fr
 ***************************************************************************/
"""

from PyQt5.QtWidgets import QFileDialog, QMessageBox, QWidget
from qgis.core import *
from osgeo import ogr, osr
import sys, os, os.path, sqlite3, shutil, csv, subprocess, unicodedata
import processing
from processing.tools import dataobjects
from .boite import Boite
from .liste_faune import liste_faune,liste_flore,liste_categorie_full,liste_categorie

os.putenv("SPATIALITE_SECURITY", "relaxed")


class AssurerCompatibilite():

    def supprimer_layer(self):
        """Remet le formulaire à zéro si une couche est supprimés dans le paneau 'couche de QGIS'

        Cette fonction permet de mettre à zéro le formulaire si une couche est supprimés dans le paneau 'couche de QGIS'
        """

        self.nom.clear()
        self.description.clear()
        self.decision.clear()
        self.refei.clear()
        self.duree.clear()
        self.comboBox.clear()
        self.comboBox_2.clear()
        self.comboBox_3.clear()
        self.comboBox_4.clear()
        self.cout.clear()
        self.montant.clear()
        self.faunes.clear()
        self.flores.clear()
        self.echeances.clear()
        self.mMapLayerComboBox_4.setCurrentIndex(0)
        self.mMapLayerComboBox_3.setCurrentIndex(0)
        self.mMapLayerComboBox_3.setStyleSheet('QgsMapLayerComboBox {border: 1px solid ;border - radius: 3px; '
                                                   'background: white;padding: 1px 23px 1px 3px; min - width: 6em;'
                                                   'background - color: rgb(255, 255, 255);'
                                                   'selection - background - color: rgb(0, 170, 255)}')
        self.label_9.setStyleSheet('QLabel {color: black')

    def champs(self):
        """Remplissage des menus déroulants du formulaire

        Cette fonction sert à remplir les menus déroulants du formulaire de champs compatibles avec GeoMCE
        """

        categorie = liste_categorie_full
        self.comboBox.addItems(categorie)
        cible = [u'Continuités écologiques', u'Bruit', u'Eau', u'Biens matériels', u'Equilibres biologiques',
                 u'Espaces naturels, agricoles, forestiers, maritimes ou de loisirs', u'Facteurs climatiques',
                 u'Faune et flore', u'Habitats naturels', u'Patrimoine culturel et archéologique', u'Population',
                 u'Sites et paysages', u'Sol']
        self.comboBox_2.addItems(cible)
        unite = [u'Année(s)', u'Mois', u'Jour(s)']
        self.comboBox_3.addItems(unite)
        modalite = [u'Audit de chantier', u'Bilan/compte rendu de suivi', u'Rapport de fin de chantier', u'Autre']
        self.comboBox_4.addItems(modalite)
        flores = liste_flore
        self.flores.addItems(flores)
        faunes = liste_faune
        self.faunes.addItems(faunes)

    def disabled_champs(self):
        """Désactivation des champs du formulaire

        Cette fonction permet de désactivier les champs du formulaire si la couche proposée n'est pas valide
        Elle permet d'éviter à l'utilisateur de remplir le formulaire si sa couche n'est pas compatible avec GeomCE
        """

        self.nom.setEnabled(False)
        self.comboBox.setEnabled(False)
        self.comboBox_2.setEnabled(False)
        self.decision.setEnabled(False)
        self.refei.setEnabled(False)
        self.description.setEnabled(False)
        self.echeances.setEnabled(False)
        self.montant.setEnabled(False)
        self.cout.setEnabled(False)
        self.duree.setEnabled(False)
        self.comboBox_3.setEnabled(False)
        self.comboBox_4.setEnabled(False)
        self.faunes.setEnabled(False)
        self.flores.setEnabled(False)
        self.change_another.setEnabled(False)
        self.save.setEnabled(False)
        self.zipbutton.setEnabled(False)
        self.eff_form.setEnabled(False)
        self.affiche_form.setEnabled(False)

    def enabled_champs(self):
        """Activation des champs du formulaire

        Cette fonction permet d'activier les champs du formulaire si la couche proposée est valide
        Elle permet à l'utilisateur de remplir le formulaire si sa couche est compatible avec GeomCE
        """

        self.nom.setEnabled(True)
        self.comboBox.setEnabled(True)
        self.comboBox_2.setEnabled(True)
        self.decision.setEnabled(True)
        self.refei.setEnabled(True)
        self.description.setEnabled(True)
        self.echeances.setEnabled(True)
        self.montant.setEnabled(True)
        self.cout.setEnabled(True)
        self.duree.setEnabled(True)
        self.comboBox_3.setEnabled(True)
        self.comboBox_4.setEnabled(True)
        self.faunes.setEnabled(True)
        self.flores.setEnabled(True)
        self.change_another.setEnabled(True)
        self.save.setEnabled(True)
        self.zipbutton.setEnabled(True)
        self.eff_form.setEnabled(True)
        self.affiche_form.setEnabled(True)


    def verif_validite(self):
        """Processus de lancement de la vérification de la géométrie de la couche

        Cette fonction permet de lancer le processus de vérification de la géométrie de la couche proposée
        """

        processing.execAlgorithmDialog('qgis:checkvalidity')

    def validite(self):
        """Processus de vérification de la géométrie de la couche

        Cette fonction permet de vérifier que la géométrie de la couche proposée est valide
        """

        layer = self.mMapLayerComboBox_3.currentLayer()

        if layer is None :
            pass
        else :
            validity = processing.run('qgis:checkvalidity',{'ERROR_OUTPUT' : 'TEMPORARY_OUTPUT',
                                                            'IGNORE_RING_SELF_INTERSECTION' : False, \
                                        'INPUT_LAYER' : layer, 'INVALID_OUTPUT' : 'TEMPORARY_OUTPUT', 'METHOD' : 2,
                                                            'VALID_OUTPUT' : 'TEMPORARY_OUTPUT'})
            layer_invalid = validity['ERROR_OUTPUT']
            invalid_count = layer_invalid.featureCount()
            return(invalid_count)


    def compatibilite(self):
        """Vérification de la compatibilité de la couche proposée avec l'import dans GeoMCE

        Cette fonction permet de vérifier :
        - si la couche est un shapefile
        - si la couche est en projection 2154
        - si la couche n'a qu'une seule entité
        - si la couche un polygon ou un multipolygon ou une ligne ou une multiligne ou un point (MAIS PAS un multipoint) sans dimensionZ
        - si la couche n'a pas de géométrie invalide
        - si la couche est en UTF-8
        - si la couche présente les champs compatibles avec GeoMCE
        - si la couche des communes est compatible
        """

        self.nom.clear()
        self.description.clear()
        self.decision.clear()
        self.refei.clear()
        self.duree.clear()
        self.comboBox.clear()
        self.comboBox_2.clear()
        self.comboBox_3.clear()
        self.comboBox_4.clear()
        self.cout.clear()
        self.montant.clear()
        self.faunes.clear()
        self.flores.clear()
        self.echeances.clear()
        layer = self.mMapLayerComboBox_3.currentLayer()
        self.champs()


        if layer is None :
            # si pas de couche : les champs et boutons sont désactivés
            self.mMapLayerComboBox_3.setStyleSheet('QgsMapLayerComboBox {border: 1px solid ;border - radius: 3px; '
                                                   'background: white;padding: 1px 23px 1px 3px; min - width: 6em;'
                                                   'background - color: rgb(255, 255, 255);'
                                                   'selection - background - color: rgb(0, 170, 255)}')
            self.label_9.setStyleSheet('QLabel {color: black')
            self.label_27.hide()
            self.label_28.show()
            self.disabled_champs()
            self.create_new_field_2.setEnabled(False)
            self.change_another.setEnabled(False)
            self.save.setEnabled(False)
            self.dossier.setEnabled(False)
            self.show_t.setEnabled(False)
            pass


        else :
            # si une couche est active, vérification de la couche
            source_chemin_couche = layer.dataProvider().dataSourceUri()
            (head, tail) = os.path.split(source_chemin_couche)
            (name, ext) = os.path.splitext(tail)
            format = ext.split('|',1)

            if format[0]!=('.shp') or "Vector" not in str(layer.type()) :
                # vérification de l'extension de la couche
                self.label_27.hide()
                self.label_28.show()
                self.mMapLayerComboBox_3.setStyleSheet('QgsMapLayerComboBox {border: 2px solid #FF0000}')
                self.label_9.setStyleSheet('QLabel {color:#FF0000}')
                self.disabled_champs()
                self.create_new_field_2.setEnabled(False)
                self.dossier.setEnabled(True)
                self.show_t.setEnabled(True)
                Boite.message(self, u"La couche n'est pas un shp mais un " + str(format[0]),
                              u" -> Chargez une couche shapefile").exec_()
                return
            else:
                pass

            lyrCRS = layer.crs().authid()
            nbfeature = layer.featureCount()


            if str(layer.wkbType()) == "WkbType.MultiPoint" or str(layer.wkbType()) == "WkbType.MultiPointZM" or \
                    str(layer.wkbType()) == "WkbType.MultiPointM":
                # vérification que la géométrie ne soit pas un multipoint
                self.label_27.hide()
                self.label_28.show()
                self.mMapLayerComboBox_3.setStyleSheet('QgsMapLayerComboBox {border: 2px solid #FF0000}')
                self.label_9.setStyleSheet('QLabel {color:#FF0000}')
                self.disabled_champs()
                self.create_new_field_2.setEnabled(False)
                self.dossier.setEnabled(True)
                self.show_t.setEnabled(True)
                Boite.message(self, u"La couche a une géométrie " + str(layer.wkbType()),
                              u" -> Chargez une couche avec une géométrie simple et non multipoint").exec_()
                return
            else:
                pass

            if nbfeature != 1:
                # vérification du nombre d'entité
                self.label_27.hide()
                self.label_28.show()
                self.mMapLayerComboBox_3.setStyleSheet('QgsMapLayerComboBox {border: 2px solid #FF0000}')
                self.label_9.setStyleSheet('QLabel {color:#FF0000}')
                self.disabled_champs()
                self.create_new_field_2.setEnabled(False)
                self.dossier.setEnabled(True)
                self.show_t.setEnabled(True)
                Boite.message(self, u"La couche présente " + str(nbfeature) + u" entités",
                              u" -> Si elle en présente plus d'une, Vous pouvez utiliser la fonction supplémentaire 'Séparer une couche vecteur'").exec_()
                return
            else:
                pass

            if lyrCRS != 'EPSG:2154':
                # vérification de la projection de la couche
                self.label_27.hide()
                self.label_28.show()
                self.mMapLayerComboBox_3.setStyleSheet('QgsMapLayerComboBox {border: 2px solid #FF0000}')
                self.label_9.setStyleSheet('QLabel {color:#FF0000}')
                self.disabled_champs()
                self.create_new_field_2.setEnabled(False)
                self.dossier.setEnabled(True)
                self.show_t.setEnabled(True)
                Boite.message(self, u"La couche n'est pas en EPSG:2154 mais en " + str(lyrCRS),
                              u" -> Reprojeter la couche en 2154 RGF93 v1 / Lambert-93").exec_()
                return
            else :
                pass

            if self.validite() > 0 :
                # vérification de la validité de la géométrie
                self.label_27.hide()
                self.label_28.show()
                self.mMapLayerComboBox_3.setStyleSheet('QgsMapLayerComboBox {border: 2px solid #FF0000}')
                self.label_9.setStyleSheet('QLabel {color:#FF0000}')
                self.disabled_champs()
                self.create_new_field_2.setEnabled(False)
                self.dossier.setEnabled(True)
                self.show_t.setEnabled(True)
                Boite.message(self, u"La couche présente " + str(self.validite()) +
                                      u" erreur(s) de géométrie",
                                      u" -> Modifiez la couche").exec_()
                return
            else :
                pass

            # vérification de l'encodage en UTF-8
            for dossier, sous_dossiers, fichiers in os.walk(head):
                for fichier in fichiers:
                    nom_table, extension = os.path.splitext(fichier)
                    if nom_table == name :
                        if extension == ".cpg" or extension == '.CPG':
                            temp_shp = os.path.join(dossier, fichier)
                            with open(temp_shp, "r") as fichier:
                                fichier_entier = fichier.read()
                                files = fichier_entier.split("\n")
                                if 'UTF-8' not in files:
                                    self.label_27.hide()
                                    self.label_28.show()
                                    self.mMapLayerComboBox_3.setStyleSheet(
                                        'QgsMapLayerComboBox {border: 2px solid #FF0000}')
                                    self.label_9.setStyleSheet('QLabel {color:#FF0000}')
                                    self.disabled_champs()
                                    self.create_new_field_2.setEnabled(False)
                                    self.change_another.setEnabled(False)
                                    self.save.setEnabled(False)
                                    self.dossier.setEnabled(True)
                                    self.show_t.setEnabled(True)
                                    Boite.message(self, u"La couche n'est pas en UTF-8 mais en " + str(files),
                                                   u" -> Modifier l'encodage de la couche").exec_()
                                    return
                                else :
                                    pass

            # vérification de la présence des champs compatible GeoMCE
            list_attendue = ['id', 'nom', 'categorie', 'cible', 'descriptio', 'decision', 'refei', 'duree', 'unite', 'modalite', 'cout', 'montant_pr', 'faunes', 'flores', 'echeances', 'communes']
            list_namefield = []
            for field in layer.fields():
                list_namefield.append(field.name())

            if list_namefield != list_attendue :
                self.label_27.hide()
                self.label_28.show()
                self.mMapLayerComboBox_3.setStyleSheet('QgsMapLayerComboBox {border: 2px solid #FF0000}')
                self.label_9.setStyleSheet('QLabel {color:#FF0000}')
                self.disabled_champs()
                self.create_new_field_2.setEnabled(True)
                self.change_another.setEnabled(False)
                self.save.setEnabled(False)
                Boite.message(self, u"La couche comporte les champs : " + str(list_namefield),
                              u"Pour être compatible, elle doit comporter les champs : " + str(list_attendue)
                              + u"\n \n -> Utilisez la fonction 'Création des champs compatibles'").exec_()
                return
            else :
                pass



            # vérification de la couche des communes dans les paramètres
            val15 = self.get_communes()

            # vérification de la présence d'une couche des communes dans les paramètres
            if not val15:
                self.label_27.hide()
                self.label_28.show()
                self.mMapLayerComboBox_3.setStyleSheet('QgsMapLayerComboBox {border: 2px solid #FF0000}')
                self.label_9.setStyleSheet('QLabel {color:#FF0000}')
                self.disabled_champs()
                self.create_new_field_2.setEnabled(False)
                self.dossier.setEnabled(True)
                self.show_t.setEnabled(True)
                Boite.message(self, u"Aucune couche des communes n'est paramétrée",
                              u" -> Ajoutez une couche des communes dans l'onglet 'Paramètres supplémentaires'").exec_()
                self.tabWidget.setCurrentWidget(self.tab_3)
                self.mMapLayerComboBox_3.setAllowEmptyLayer(True)
                self.mMapLayerComboBox_3.setCurrentIndex(0)
                return
            else :
                pass

            # vérification que la couche des communes intersecte la couche

            dest = QgsProcessingParameterVectorDestination(name="com")
            destcom = dest.generateTemporaryDestination()

            layer_communes_region = QgsVectorLayer(val15, "communes", "ogr")
            extraction = processing.run('native:extractbylocation',
                                        {'INPUT': layer_communes_region, 'INTERSECT': layer, 'PREDICATE': [0],
                                         'OUTPUT': destcom})
            layer_extraction = QgsVectorLayer(destcom, "output", "ogr")
            type = layer_extraction.dataProvider().fieldNameIndex('TYPE')
            insee = layer_extraction.dataProvider().fieldNameIndex('INSEE_COM')
            dep = layer_extraction.dataProvider().fieldNameIndex('INSEE_DEP')
            compare_field_index = type
            concat_field_index = insee
            new_field_index = dep
            list_com = []
            list_id = []
            feature_dict = {f.id(): f for f in layer_extraction.getFeatures()}

            if not feature_dict:
                self.label_27.hide()
                self.label_28.show()
                self.mMapLayerComboBox_3.setStyleSheet('QgsMapLayerComboBox {border: 2px solid #FF0000}')
                self.label_9.setStyleSheet('QLabel {color:#FF0000}')
                self.disabled_champs()
                self.create_new_field_2.setEnabled(False)
                self.dossier.setEnabled(True)
                self.show_t.setEnabled(True)
                Boite.message(self, u"La couche des communes n'intersecte pas la couche de la mesure ",
                              u" -> Vérifier la couche des communes dans l'onglet 'Paramètres supplémentaires'").exec_()
                self.tabWidget.setCurrentWidget(self.tab_3)
                self.mMapLayerComboBox_3.setAllowEmptyLayer(True)
                self.mMapLayerComboBox_3.setCurrentIndex(0)
                return
            else :
                # Si tout est ok, activation des champs et des boutons et remplissage automatique du formulaire
                # si il est déjà remplie en conséquence
                self.remplir_champs_exts()

    def remplir_champs_exts(self):
        """Remplissage automatique des champs du formulaire

        Si la couche chargée est compatible et si un ou des champs ont déjà été remplis, le formulaire est rempli
        en conséquence
        """

        layer = self.mMapLayerComboBox_3.currentLayer()
        self.label_28.hide()
        self.label_27.show()
        self.create_new_field_2.setEnabled(True)
        self.change_another.setEnabled(True)
        self.save.setEnabled(True)
        self.dossier.setEnabled(True)
        self.show_t.setEnabled(True)
        self.enabled_champs()
        self.mMapLayerComboBox_3.setStyleSheet('QgsMapLayerComboBox {border: 2px solid #a5d50c}')
        self.label_9.setStyleSheet('QLabel {color:#a5d50c}')
        features = layer.getFeatures()

        for feat in features:
            attrs = feat.attributes()
        self.nom.setText(str(attrs[1]))

        categorie = liste_categorie
        if str(attrs[2]) =='NULL' :
            self.comboBox.setCurrentIndex(0)
        else :
            index_cat = categorie.index(str(attrs[2]))
            self.comboBox.setCurrentIndex(index_cat)

        cible = [u'Continuités écologiques',u'Bruit',u'Eau',u'Biens matériels',u'Equilibres biologiques',
                 u'Espaces naturels, agricoles, forestiers, maritimes ou de loisirs',u'Facteurs climatiques',
                 u'Faune et flore',u'Habitats naturels',u'Patrimoine culturel et archéologique',u'Population',
                 u'Sites et paysages',u'Sol']

        if str(attrs[3]) =='NULL' :
            pass
        else :
            list_cible2 = str(attrs[3]).split('|')
            for cible2 in list_cible2 :

                index_cible = cible.index(cible2)
                self.comboBox_2.setItemCheckState(index_cible,2)

        self.description.setPlainText(str(attrs[4]))
        self.decision.setText(str(attrs[5]))
        self.refei.setText(str(attrs[6]))

        if str(attrs[7]) =='NULL' :
            pass
        else :
            self.duree.setValue(int(attrs[7]))

        unite = [u'Année', u'Mois', u'Jour(s)']
        if str(attrs[8]) == 'NULL':
            self.comboBox_3.setCurrentIndex(0)
        else:
            index_unite = unite.index(str(attrs[8]))
            self.comboBox_3.setCurrentIndex(index_unite)

        mod_suivi = [u'Audit de chantier',u'Bilan/compte rendu de suivi',u'Rapport de fin de chantier',u'Autre']
        if str(attrs[9]) == 'NULL':
            self.comboBox_4.setCurrentIndex(0)
        else:
            index_mod_suivi = mod_suivi.index(str(attrs[9]))
            self.comboBox_4.setCurrentIndex(index_mod_suivi)

        if str(attrs[10]) =='NULL' :
            pass
        else :
            self.cout.setValue(int(attrs[10]))

        if str(attrs[11]) == 'NULL':
            pass
        else:
            self.montant.setValue(int(attrs[11]))

        self.echeances.setText(str(attrs[14]))

        faunes = liste_faune

        if str(attrs[12]) == 'NULL' or not str(attrs[12]):
            pass
        else:
            list_faunes2 = str(attrs[12]).split('|')
            for faunes2 in list_faunes2:
                index_faunes = faunes.index(faunes2)
                self.faunes.setItemCheckState(index_faunes, 2)

        flores = liste_flore

        if str(attrs[13]) == 'NULL' or not str(attrs[13]):
            pass
        else:
            list_flores2 = str(attrs[13]).split('|')
            for flores2 in list_flores2:
                index_flores = flores.index(flores2)
                self.flores.setItemCheckState(index_flores, 2)