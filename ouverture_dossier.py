# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GeoMCE version 2.0
                                 A QGIS plugin
 GeoMCE permet de mettre en forme des couches shape en vue des les impoprter
 dans la plateforme nationale de géolocalisation des mesures compensatoires
 environnementales prévue par la loi sur la reconquète de la biodiveristé et
 des paysages du 16 aout 2016
                              -------------------
        begin                : 2020-03-26
        git sha              : $Format:%H$
        copyright            : (C) 2020 by DREAL Auvergne-Rhône-Alpes - 2024 by Cerema Centre-Est
        email                : antoine.lemot@cerema.fr
 ***************************************************************************/
"""

from PyQt5.QtWidgets import QFileDialog, QMessageBox
from qgis.core import *
from osgeo import ogr, osr
import sys, os, os.path, sqlite3, shutil, csv, subprocess, unicodedata
import processing
from processing.tools import dataobjects

os.putenv("SPATIALITE_SECURITY", "relaxed")


class Ouvrir_Dossier():

    # ouvre le dossier contenant les fichiers de la couche a traiter...
    def dossier_f(self):
        """Ouverture du dossier contenat les fichiers de la couche traitée

        (fonction déclenchée par bouton 'Ouvrir le dossier' dans 'Mise en forme de vecteurs existants')
        """
        layername = self.mMapLayerComboBox_3.currentLayer()
        doudai = layername.dataProvider().dataSourceUri()
        path = os.path.dirname(doudai)
        os.startfile(path)

    def dossier_2_f(self):
        path = self.mQgsFileWidget.text()
        path_fusion = os.path.dirname(path)
        os.startfile(path_fusion)