# -*- coding: utf-8 -*-

"""
/***************************************************************************
 GeoMCE version 2.0
                                 A QGIS plugin
 GeoMCE permet de mettre en forme des couches shape en vue des les impoprter
 dans la plateforme nationale de géolocalisation des mesures compensatoires
 environnementales prévue par la loi sur la reconquète de la biodiveristé et
 des paysages du 16 aout 2016
                              -------------------
        begin                : 2020-03-26
        git sha              : $Format:%H$
        copyright            : (C) 2020 by DREAL Auvergne-Rhône-Alpes - 2024 by Cerema Centre-Est
        email                : antoine.lemot@cerema.fr
 ***************************************************************************/
"""

from PyQt5 import QtCore
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import QDialog, QMessageBox
from qgis.core import *

import os.path

from .gc_utils import (load_ui)
from .GeoMCE_dialog import GeoMCEDialog
from .fonctions_pers import Separer_Couches,Ouvrir_Wfs, Fusionner_Couches
from .compatibilite import AssurerCompatibilite
from .formulaire import Remplir_Formulaire
from .creation_champs import Creer_Champs
from .show_table import Montrer_Table
from .zipp import Zipper
from .ouverture_dossier import Ouvrir_Dossier
from .code_insee import Ecrire_Codeinsee, Recuperer_Codeinsee
from .open_about import Ouvrir_About
from .get_champs import Champs

pluginPath = os.path.dirname(__file__)
FORM_CLASS = load_ui('GeoMCE_dialog_base')


class AppelFonction(QDialog,QPixmap, FORM_CLASS, Separer_Couches,Ouvrir_Wfs,Fusionner_Couches,AssurerCompatibilite,
                    Remplir_Formulaire,Creer_Champs,Montrer_Table,Zipper,Ouvrir_Dossier,Ecrire_Codeinsee,
                    Recuperer_Codeinsee, Ouvrir_About,Champs):

    def __init__(self, iface, parent=None):
        QDialog.__init__(self, parent)
        self.setupUi(self)
        self.iface = iface

        QgsProject.instance().layerRemoved.connect(self.supprimer_layer)

####### Mise en forme de vecteurs existants #######
        self.mMapLayerComboBox_3.setAllowEmptyLayer(True)
        self.mMapLayerComboBox_3.setCurrentIndex(0)
        self.mMapLayerComboBox_3.layerChanged.connect(self.compatibilite)
        self.create_new_field_2.clicked.connect(self.create_new_field_2_f)
        self.change_another.clicked.connect(self.change_to_any)
        self.save.clicked.connect(self.save_edits)
        self.show_t.clicked.connect(self.show_table)
        self.zipbutton.clicked.connect(self.archive)
        self.dossier.clicked.connect(self.dossier_f)
        self.eff_form.clicked.connect(self.eff_formulaire)
        self.affiche_form.clicked.connect(self.remplir_champs_exts)

####### Fonctionnalités supplémentaires #######

        ### pour la séparation de couche, seules les couches chargées dans QGIS et avec plus d'une entité sont affichées dans la MapLayerCombobax

        map_layers = QgsProject.instance().mapLayers().values()
        allow_list = [
            lyr.id() for lyr in map_layers if lyr.type() == QgsMapLayer.VectorLayer and lyr.featureCount() > 1]
        except_list = [l for l in map_layers if l.id() not in allow_list]
        self.mMapLayerComboBox_4.setExceptedLayerList(except_list)

        self.mMapLayerComboBox_4.setAllowEmptyLayer(True)
        self.mMapLayerComboBox_4.setCurrentIndex(0)

        self.mFieldComboBox_2.setEnabled(False)
        self.enregistrement.setEnabled(False)
        self.create_new_field.setEnabled(False)
        self.pushButton_3.setEnabled(False)

        self.mMapLayerComboBox_4.layerChanged.connect(self.enabled_separer)
        self.mMapLayerComboBox_4.layerChanged.connect(self.champ_sep)

        ### pour la séparation des couches, la listes des choix et mise à jour
        # lorsqu'une couche est ajoutée ou supprimée de QGIS
        QgsProject.instance().layersAdded.connect(self.listecouches_separer)
        QgsProject.instance().layersRemoved.connect(self.listecouches_separer)


        ### pour la fusion des couches, les couches uniqement vecteurs sont chargées dans la liste de choix
        layers = [layer for layer in QgsProject.instance().mapLayers().values()]
        for layer in layers:
            layerType = layer.type()
            if layerType == QgsMapLayer.VectorLayer:
                self.mComboBox.addItem(layer.name())

        self.enregistrement_fusion.setEnabled(False)
        self.pushButton_4.setEnabled(False)
        self.fusion.setEnabled(False)

        self.mComboBox.checkedItemsChanged.connect(self.enabled_fusioner)

        ### pour la fusion des couches, la listes des choix et mise à jour
        # lorsqu'une couche est ajoutée ou supprimée de QGIS
        QgsProject.instance().layersAdded.connect(self.listecouches_fusion)
        QgsProject.instance().layersRemoved.connect(self.listecouches_fusion)

        self.geoportail.clicked.connect(self.wfs)
        self.pushButton_3.clicked.connect(self.select_save_folder)
        self.create_new_field.clicked.connect(self.create_new_field_f)



        self.pushButton_4.clicked.connect(self.select_save_folder_2)
        self.fusion.clicked.connect(self.fusion_f)
        # self.show_t_2.clicked.connect(self.show_table_2)
        # self.zipbutton_2.clicked.connect(self.archive_2)
        # self.dossier_2.clicked.connect(self.dossier_2_f)
        # self.Exit_2.clicked.connect(self.exit)

####### Paramètres de l'extension #######
        valcom = self.get_communes()
        self.communes.setText(valcom)
        self.pushButton.clicked.connect(self.select_output_file_store)

####### About #######
        self.about.clicked.connect(self.doabout)

# ####### remplit les listes deroulantes #######
        self.disabled_champs()
        self.create_new_field_2.setEnabled(False)
        self.change_another.setEnabled(False)
        self.save.setEnabled(False)
        self.dossier.setEnabled(False)
        self.show_t.setEnabled(False)









