# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GeoMCE version 2.0
                                 A QGIS plugin
 GeoMCE permet de mettre en forme des couches shape en vue des les impoprter
 dans la plateforme nationale de géolocalisation des mesures compensatoires
 environnementales prévue par la loi sur la reconquète de la biodiveristé et
 des paysages du 16 aout 2016
                              -------------------
        begin                : 2020-03-26
        git sha              : $Format:%H$
        copyright            : (C) 2020 by DREAL Auvergne-Rhône-Alpes - 2024 by Cerema Centre-Est
        email                : antoine.lemot@cerema.fr
 ***************************************************************************/
"""

import sys,os
from PyQt5 import QtCore, QtGui, QtWidgets, QtWebKitWidgets,QtWebKit
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtWebKit import *
from PyQt5.QtWebKitWidgets import QWebView
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig)

class Ui_About(object):

    def setupUi(self, About):
        About.setObjectName(_fromUtf8("A propos"))
        About.resize(950, 900)
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)
        font = QtGui.QFont()
        font.setPointSize(9)
        self.label_2 = QtWidgets.QLabel(About)
        self.label_2.setGeometry(QtCore.QRect(0, 0, 950, 131))
        self.label_2.setText(_fromUtf8(""))
        self.label_2.setPixmap(QtGui.QPixmap(_fromUtf8(":/plugins/GeoMCE_v2/img/geomce.png")))
        self.label_2.setScaledContents(True)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtWidgets.QPushButton(About)
        self.label_3.setGeometry(QtCore.QRect(300, 141, 100, 50))
        self.label_3.setText(_fromUtf8("Retour à la notice"))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.webView = QtWebKitWidgets.QWebView(About)
        file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "aide.html"))
        self.webView.setGeometry(QtCore.QRect(20, 200, 910, 730))
        self.webView.setProperty("url", QUrl.fromLocalFile(file_path))
        self.webView.setProperty("zoomFactor", 1)
        self.webView.setObjectName(_fromUtf8("webView"))
        QtCore.QMetaObject.connectSlotsByName(About)

        self.label_4 = QtWidgets.QPushButton(About)
        self.label_4.setGeometry(QtCore.QRect(500, 141, 110, 50))
        self.label_4.setText(_fromUtf8("Télécharger la notice"))
        self.label_4.setObjectName(_fromUtf8("label_4"))


