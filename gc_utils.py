# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Mnefzh version 2
                                 A QGIS plugin
 Méthode nationale d'évaluation des fonctionnalités des zones humides
                              -------------------
        begin                : 2023-04-26
        git sha              : $Format:%H$
        copyright            : (C) 2023 by Antoine Lemot
        email                : antoine.lemot@cerema.fr
 ***************************************************************************/
"""

import os
from PyQt5 import (QtCore, uic)
from qgis.PyQt.QtGui import QIcon

# This import is to enable SIP API V2
# noinspection PyUnresolvedReferences
import qgis  # pylint: disable=unused-import


def load_ui(name):
    ui_file = os.path.join(os.path.dirname(os.path.realpath(__file__)),name + '.ui')
    return uic.loadUiType(ui_file)[0]


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
    except TypeError:
        return False


def resources_path(*args):
    """Get the path to our resources folder.
    """
    path = os.path.dirname(__file__)
    path = os.path.abspath(
        os.path.join(path, 'resources'))
    for item in args:
        path = os.path.abspath(os.path.join(path, item))
    return path


def set_icon(btn, icon_file):
    ipath = resources_path('img', icon_file)
    # print ipath
    btn.setIcon(QIcon(ipath))


def resource_url(path):
    """Get the a local filesystem url to a given resource.
    """
    url = QtCore.QUrl.fromLocalFile(path)
    return str(url.toString())


